package nils.jacobsen.msiscore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsisCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsisCoreApplication.class, args);
    }

}
